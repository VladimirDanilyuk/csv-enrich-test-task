package ru.rgs.csvparser.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.FeignException;
import feign.codec.ErrorDecoder;

import lombok.extern.slf4j.Slf4j;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ru.rgs.csvparser.client.ScoringClient;
import ru.rgs.csvparser.domain.Response;
import ru.rgs.csvparser.exception.ClientProcessingException;
import ru.rgs.csvparser.service.CsvParserService;
import ru.rgs.csvparser.service.impl.CsvParserServiceImpl;

import java.io.IOException;

@Configuration
@Slf4j
public class MainConfiguration {

    @Bean
    public CsvParserService csvParserService(ScoringClient client, @Value("${delimiter:,}") String delimiter) {
        return new CsvParserServiceImpl(client, delimiter);
    }

    @Bean
    public ErrorDecoder errorDecoder(ObjectMapper mapper) {
        return (methodKey, feignResponse) -> {
            if (feignResponse.status() == HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                try {
                    Response response = mapper.readValue(feignResponse.body().asReader(), Response.class);
                    return new ClientProcessingException(response, feignResponse.reason());
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
            return FeignException.errorStatus(methodKey, feignResponse);
        };
    }
}
